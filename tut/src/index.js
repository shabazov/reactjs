import React from 'react';
import ReactDom from 'react-dom';
import './index.css';

const books = [
  {
    image: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9788/4910/9788491050209.jpg',
    title: 'Moby Dick',
    author: 'Herman Melville'
  },
  {
    image: 'https://m.media-amazon.com/images/I/519Nw0Uw+jL._SY346_.jpg',
    title: 'A song of ice and fire',
    author: 'George R.R. Martin'
  },
]

const names = ['Tania', 'Leti', 'Egor'];
const newNames = names.map((name) => {
  return <h1>{name}</h1>;
});

function BookList() {
  return (
    <section>
      <h1 className='listTitle'>This is a BookList</h1>
      <div className='booklist'>
       {newNames}
      </div>
    </section>
  );
};

// const Book = ({image, title, author}) => {
  
//   // const {image, title, author} = props;
//   return (
//     <article className='book'>
//       <div className='image'>
//         <img src={image} alt="" />
//       </div>
//       <h2>{ title }</h2>
//       <small>by:</small>
//       <h3>{ author }</h3>  
//     </article>
//   );
// };


ReactDom.render(<BookList />, document.getElementById('root'));